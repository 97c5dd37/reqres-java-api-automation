package com.reqres.models;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SingleEntryResponse<T> {
    @JsonProperty("data")
    public T entry;
}
