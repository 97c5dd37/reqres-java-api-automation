package com.reqres.models;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.ArrayList;

public class MultiEntryResponse<T> implements Serializable {
    @JsonProperty("page")
    public int page;
    @JsonProperty("per_page")
    public int perPage;
    @JsonProperty("total")
    public int total;
    @JsonProperty("total_pages")
    public int totalPages;
    @JsonProperty("data")
    public ArrayList<T> entries;
}
