package com.reqres.models;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Resource {
    @JsonProperty("id")
    public int id;
    @JsonProperty("name")
    public String name;
    @JsonProperty("year")
    public int year;
    @JsonProperty("color")
    public String color;
    @JsonProperty("pantone_value")
    public String pantoneValue;
}
