package com.reqres.api;

import com.reqres.models.MultiEntryResponse;
import com.reqres.models.Resource;
import com.reqres.models.SingleEntryResponse;
import com.reqres.models.User;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ReqRes {

    @GET("users")
    Call<MultiEntryResponse> listUsers(@Query("page") int pageNumber);

    @GET("users/{id}")
    Call<SingleEntryResponse<User>> getUser(@Path("id") int userId);

    @GET("unknown")
    Call<MultiEntryResponse> listResources();

    @GET("unknown/{id}")
    Call<SingleEntryResponse<Resource>> getResource(@Path("id") int resourceId);
}
