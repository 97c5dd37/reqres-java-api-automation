package com.reqres.api;

import com.reqres.Options;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

public class ReqResApi {
    private ReqRes reqRes;
    private static ReqResApi instance;

    private ReqResApi() {
        final Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Options.SITE_URL)


                .addConverterFactory(JacksonConverterFactory.create())
                .build();
        reqRes = retrofit.create(ReqRes.class);
    }

    public static void init() {
        instance = new ReqResApi();
    }

    public static ReqRes getApi() {
        return instance.reqRes;
    }

    public void setApi(ReqRes starWarsApi) {
        instance.reqRes = starWarsApi;
    }
}
