import com.reqres.api.ReqRes;
import com.reqres.api.ReqResApi;
import com.reqres.models.MultiEntryResponse;
import com.reqres.models.Resource;
import com.reqres.models.SingleEntryResponse;
import com.reqres.models.User;
import org.junit.Before;
import org.junit.Test;
import retrofit2.Response;

import java.io.IOException;
import java.util.ArrayList;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNot.not;
import static org.hamcrest.core.IsNull.nullValue;
import static org.hamcrest.junit.MatcherAssert.assertThat;

public class UsersTest {
    private ReqRes reqRes = null;

    @Before
    public void Before() {
        ReqResApi.init();
        reqRes = ReqResApi.getApi();
    }

    @Test
    public void GetAllUsers() throws IOException {
        Response response = reqRes.listUsers(2).execute();
        assertThat(response, is(not(nullValue())));
        assertThat(response.isSuccessful(), is(true));
        MultiEntryResponse multiEntryResponse = (MultiEntryResponse) response.body();
        assertThat(multiEntryResponse, is(not(nullValue())));
        assertThat(multiEntryResponse.page, is(2));
        assertThat(multiEntryResponse.perPage, is(3));
    }

    @Test
    public void GetSingleUser() throws IOException {
        Response response = reqRes.getUser(2).execute();
        assertThat(response, is(not(nullValue())));
        assertThat(response.isSuccessful(), is(true));
        Object g = response.body();
        SingleEntryResponse<User> entryResponse = (SingleEntryResponse<User>) response.body();
        assertThat(entryResponse, is(not(nullValue())));
        User user = entryResponse.entry;
        assertThat(user, is(not(nullValue())));
        assertThat(user.firstName, is("Janet"));
        assertThat(user.lastName, is("Weaver"));
    }

    @Test
    public void GetBadSingleUser() throws IOException {
        Response response = reqRes.getUser(23).execute();
        assertThat(response, is(not(nullValue())));
        assertThat(response.isSuccessful(), is(false));
    }

    @Test
    public void GetResources() throws IOException {
        Response response = reqRes.listResources().execute();
        assertThat(response, is(not(nullValue())));
        assertThat(response.isSuccessful(), is(true));

        MultiEntryResponse multiEntryResponse = ((MultiEntryResponse) response.body());
        assertThat(multiEntryResponse, is(not(nullValue())));
        assertThat(multiEntryResponse.page, is(1));
        int usersPerPage = 3;
        assertThat(multiEntryResponse.perPage, is(usersPerPage));
        ArrayList<User> users = multiEntryResponse.entries;
        assertThat(users.size(), is(usersPerPage));
    }


    @Test
    public void GeSingletResource() throws IOException {
        Response response = reqRes.getResource(2).execute();
        assertThat(response, is(not(nullValue())));
        assertThat(response.isSuccessful(), is(true));
        assertThat(response, is(not(nullValue())));
        Resource resource = ((SingleEntryResponse<Resource>) response.body()).entry;
        assertThat(resource, is(not(nullValue())));
        assertThat(resource.id, is(2));
    }

    @Test
    public void GetBadResource() throws IOException {
        Response response = reqRes.getResource(23).execute();
        assertThat(response, is(not(nullValue())));
        assertThat(response.isSuccessful(), is(false));
    }

}
